#include <RcppArmadillo.h>

// [[Rcpp::depends(RcppArmadillo)]]
using namespace arma;


// [[Rcpp::export]]
arma::ivec trim_protos(arma::mat protos, arma::ivec keep_protos,
                       arma::uvec num_vars, arma::uvec cat_vars,
                       unsigned int k) {
  
  double d1 = 0;
  double d2 = 0;
  
  arma::mat protos_num = protos.cols(num_vars);
  arma::mat protos_cat = protos.cols(cat_vars);

  for (unsigned int l=0; l < k-1; l++) {
    for (unsigned int m=l+1; m < k; m++) {
  
      d1 = norm(protos_num.row(l) - protos_num.row(m), 2);
      d2 = sum(protos_cat.row(l) != protos_cat.row(m));
      
      if (d1 + d2 == 0) {
        keep_protos(m) = 0;
      }
    }
  
  }
  
  return keep_protos;

}


// [[Rcpp::export]]
arma::mat compute_distances(arma::mat x, arma::mat protos, arma::vec lambda,
                            arma::uvec num_vars, arma::uvec cat_vars,
                            unsigned int k) {
  
  int n_lambda = lambda.size();
  int n_x = x.n_rows;
  
  arma::mat x_num = x.cols(num_vars);
  arma::mat x_cat = x.cols(cat_vars);
  
  arma::mat d1_diff;
  arma::umat d2_diff;
  arma::vec d1, d2;
  
  arma::mat dists(n_x, k, fill::zeros);
  uvec ui(1);
  
  for (unsigned int i=0; i < k; i++) {
    
    ui(0) = i;

    d1_diff = pow(x_num - repmat(protos.submat(ui, num_vars), n_x, 1), 2);
    d2_diff = (x_cat - repmat(protos.submat(ui, cat_vars), n_x, 1) != 0);
    
    if (n_lambda == 1) {
      
      d1 = sum(d1_diff, 1);
      d2 = lambda(0) * sum(conv_to<mat>::from(d2_diff), 1);
      
    } else {
      
      d1 = d1_diff * lambda.elem(num_vars);
      d2 = d2_diff * lambda.elem(cat_vars);
      
    }
    
    dists.col(i) = d1 + d2;
    
  }
  
  return dists;
  
}