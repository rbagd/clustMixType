arma::ivec trim_protos(arma::mat protos, arma::ivec keep_protos,
                       arma::uvec num_vars, arma::uvec cat_vars,
                       unsigned int k);

arma::mat compute_distances(arma::mat x, arma::mat protos, arma::vec lambda,
                            arma::uvec num_vars, arma::uvec cat_vars,
                            unsigned int k);